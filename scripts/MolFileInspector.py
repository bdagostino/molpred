# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

########################################################################################################################
# This script is used to check if mol files have been geometry optimized


import numpy as np
import os, glob, copy

import DataReduction as dr
import MolPred as mp

root_dir = './mol'

min_atom_dist = 1.0
max_atom_dist = 40.0

mol_files = glob.glob(root_dir + '/*.mol')

# Find the files in common between all listed root directories
compmols = []
compmol_names = []
for molfile in mol_files:

    mol_name, atom_types, atompos3d = dr.parse_mol_file(molfile)
    n_atoms = len(atompos3d)
    if n_atoms == 0:
        continue

    pos3d = np.zeros((n_atoms,3))
    for i in range(n_atoms):
        pos3d[i,:] = np.squeeze(atompos3d[i])

    warnings = mp.molecule_geometry_check(pos3d, min_dist=min_atom_dist, max_dist=max_atom_dist, mol_name=mol_name)

