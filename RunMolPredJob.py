# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

########################################################################################################################
# This is a script to run a MolPred "job".  There are 3 job types that can be run:
# (1) Model Training: trains a model.  A model can be an ensemble
# (2) Model Stress Test: "stress tests" a model to determine sensitivity to settings (hyperparameters, training errors,
#                       etc.
# (3) Model Inference: runs previously trained model(s) to generate predictions on molecules.  Can ensemble arbitrarily
#                       many models to generate composite predictions
#
# All settings for each job type are contained in the control .xml file given as input.  The job type is in the header
#   of the xml control file

import argparse
import os
import xml.etree.ElementTree as et
import JobLauncher as jl
import MolPred as mp

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Runs a MolPred job (train a model, model stress test, or model '
                                                 'inference), given the name of a configuration file to parse')
    parser.add_argument('--config_file', default='./config/Inference_Control.xml')
    args = parser.parse_args()
    config_file = args.config_file

    # Determine the compute environment for the job
    tree = et.parse(config_file)
    root = tree.getroot()
    job_type = root.attrib['job_type']

    # Run the job in the compute environment specified by the config file.  The job priority is:
    # (1) Check for AWS job first
    #   - an AWS job will be run if the run_aws_job flag is set to True
    # (2) Check for running in a local Docker container
    #   - a local Docker job will be run if the run_aws_job flag is not True, and if there is is a name provided in
    #       the docker_image_name field
    # (3) Default: Run job natively (need the most dependencies- all support libraries)
    #   - a local job will be run "natively", using Python libraries installed on your local computer, if there is no
    #       aws_bucket_name and no docker_image_name provided in the config xml file.  This mode has the most dependencies

    run_aws_job = False
    if root.find('run_aws_job').text is not None:
        if root.find('run_aws_job').text.lower() == 'true':
            run_aws_job = True
            # NOTE: setting up MolPred to run on AWS is outside the scope of this project.  It is up to the user to
            #   configure AWS EC2 instances, perform container orchestration, etc.
            submitted_job = jl.run_job_aws(config_file)

    if run_aws_job is False:
        if root.find('docker_image_name').text is not None:
            docker_success = jl.run_job_docker(config_file, root.find('docker_image_name').text)
            print('Completed local Docker job')

        else:
            # Run a MolPred job natively (not in a Docker container)
            os.environ['KMP_DUPLICATE_LIB_OK'] = 'True' # In some OSs there is an OpenMP bug unless this environment variable is set
            success = mp.run_job(config_file)

