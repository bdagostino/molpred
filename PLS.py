# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'
# PLS regression / classification functions.

import numpy as np
import copy
from scipy import linalg
import warnings


pinv2_args = {'check_finite': False}

########################################################################################################################
# Function to train a PLS model
def pls_train( model, hp, X, Y, pred_type='regression'):

    if np.size(X,0) != np.size(Y,0):
        print('Mismatched # samples in predictors X and responses Y!!!')
        return([])

    if hp.n_comp < 1:
        print('Need to use at least 1 component!!!')
        return( [] )

    # If this is a classification (PLS-DA) model, need to reformat the response data to handle multiple classes
    if pred_type == 'classification':
        Y = reformat_response_plsda(Y)

    if hp.algo.lower() == 'nipals':
        model = pls_train_nipals(model, hp, X, Y, pred_type=pred_type)
    elif hp.algo.lower() == 'simpls':
        model = pls_train_simpls(model, hp, X, Y, pred_type=pred_type)
    elif hp.algo.lower() == 'svd':
        model = pls_train_svd(model, hp, X, Y)

    return( model )


########################################################################################################################
# Reformat Y output variable to multi-response format needed for PLS-DA
def reformat_response_plsda(Y):
    Nclass = np.size(np.unique(Y))
    Ns = np.size(Y,0)
    Yref = np.zeros((Ns,Nclass))

    for n in range(0, Nclass):
        curidx = np.where(Y == n)[0]
        Yref[curidx,n] = 1

    return( Yref )



########################################################################################################################
# Uses the output of a trained PLS model to compute regression coefficients that can be applied later
def compute_pls_coeffs(X_weights, X_loadings, Y_weights):

    Wstar = np.dot(X_weights, np.linalg.pinv(np.dot(X_loadings.T, X_weights)))
    coeffs = np.dot(Wstar, Y_weights.T)

    return(coeffs)


########################################################################################################################
# Trains a PLS model using the SVD method
def pls_train_svd(model, hp, X, Y):

    # Using the Abdi SVD PLS method
    XtY = np.dot(X.T,Y)
    U,s,Vt = np.linalg.svd(XtY,full_matrices=True)
    S = np.zeros((np.size(U,0),np.size(Vt,0)))
    Nsv = np.size(s)
    for n in range(0,Nsv):
        S[n,n] = s[n]

    # Restrict to the desired # components
    U = U[:,0:hp.n_comp]

    T = np.dot(X,U)
    P = np.dot(X.T, T)
    YV = np.dot(Y,Vt.T)
    B = np.dot(T.T,YV)
    Bpls = np.dot(np.linalg.pinv(P.T), np.dot(B,Vt)) # matrix of regression coefficients

    Yrecon = np.dot(X, Bpls)
    rmse = np.sqrt(np.mean((Yrecon - Y)**2.0))

    model.Bpls = copy.deepcopy(Bpls)

    return( model )


########################################################################################################################
# Applies a previously trained SVD-based PLS model to new matrix of predictors X
# def apply_pls_svd(model, X):
#
#     # mu = np.mean(X,0)
#     # for n in range(0,np.size(X,1)):
#     #     X[:,n] -= mu[n]
#
#     Ypred = np.dot(X, model.Bpls)
#
#     return( Ypred)


########################################################################################################################
# Trains a PLS predictive model using the NIPALS (Nonlinear Iterative Partial Least Squares) algorithm
#   This is the same thing as the more commonly used "power" method for computing eigenvectors
def pls_train_nipals(model, hp, Xin, Yin, pred_type = 'regression'):
    X = copy.deepcopy(Xin)
    Y = copy.deepcopy(Yin)
    Ns = X.shape[0] # num training samples
    Np = X.shape[1] # num training predictors
    Nr = Y.shape[1] # num responses

    # Scale (in place)
    if pred_type == 'regression' or 'classification':
        X, Y, model.x_mean, model.y_mean, model.x_std, model.y_std = standardize_xy(X, Y, hp.scale_data)
    else:
        model.x_mean = 0.0
        model.x_std = 1.0
        model.y_mean = 0.0
        model.y_std = 1.0

    # Residual (deflated) matrices
    Xk = X
    Yk = Y

    # Results matrices
    model.x_scores = np.zeros((Ns, hp.n_comp))
    model.y_scores = np.zeros((Ns, hp.n_comp))
    model.x_weights = np.zeros((Np, hp.n_comp))
    model.y_weights = np.zeros((Nr, hp.n_comp))
    model.x_loadings = np.zeros((Np, hp.n_comp))
    model.y_loadings = np.zeros((Nr, hp.n_comp))

    # NIPALS algo: outer loop, over components
    for k in range(hp.n_comp):
        if np.all(np.dot(Yk.T, Yk) < np.finfo(np.double).eps):
            # Yk constant
            warnings.warn('Y residual constant at iteration %s' % k)
            break
        # 1) weights estimation (inner loop)
        # -----------------------------------

        x_weights, y_weights, n_iter_ = nipals_twoblocks_inner_loop(X=Xk, Y=Yk, max_iter=hp.max_iters, tol=hp.tol, norm_y_weights=hp.norm_y_weights)
        # self.n_iter_.append(n_iter_)

        # Forces sign stability of x_weights and y_weights
        # Sign indeterminacy issue from svd if algorithm == "svd", and from platform dependent computation if algorithm == 'nipals'
        x_weights, y_weights = svd_flip(x_weights, y_weights.T)
        y_weights = y_weights.T

        # compute scores
        x_scores = np.dot(Xk, x_weights)
        if hp.norm_y_weights:
            y_ss = 1
        else:
            y_ss = np.dot(y_weights.T, y_weights)
        y_scores = np.dot(Yk, y_weights) / y_ss

        # Test for null variance
        if np.dot(x_scores.T, x_scores) < np.finfo(np.double).eps:
            warnings.warn('X scores are null at iteration %s' % k)
            break

        # 2) Deflation (in place)
        # Regress Xk's on x_score
        x_loadings = np.dot(Xk.T, x_scores) / np.dot(x_scores.T, x_scores)
        # - subtract rank-one approximations to obtain remainder matrix
        Xk -= np.dot(x_scores, x_loadings.T)
        if hp.deflation_mode == "canonical":
            # - regress Yk's on y_score, then subtract rank-one approx.
            y_loadings = (np.dot(Yk.T, y_scores)
                          / np.dot(y_scores.T, y_scores))
            Yk -= np.dot(y_scores, y_loadings.T)
        if hp.deflation_mode == "regression":
            # - regress Yk's on x_score, then subtract rank-one approx.
            y_loadings = (np.dot(Yk.T, x_scores)
                          / np.dot(x_scores.T, x_scores))
            Yk -= np.dot(x_scores, y_loadings.T)
        # 3) Store weights, scores and loadings
        model.x_scores[:, k] = x_scores.ravel()  # T
        model.y_scores[:, k] = y_scores.ravel()  # U
        model.x_weights[:, k] = x_weights.ravel()  # W
        model.y_weights[:, k] = y_weights.ravel()  # C
        model.x_loadings[:, k] = x_loadings.ravel()  # P
        model.y_loadings[:, k] = y_loadings.ravel()  # Q
    # X = TP' + Err and Y = UQ' + Err

    # 4) rotations from input space to transformed space (scores)
    # T = X W(P'W)^-1 = XW* (W* : p x k matrix)
    # U = Y C(Q'C)^-1 = YC* (W* : q x k matrix)
    model.x_rotations = np.dot(model.x_weights,linalg.pinv2(np.dot(model.x_loadings.T, model.x_weights), **pinv2_args))
    if Y.shape[1] > 1:
        model.y_rotations = np.dot(model.y_weights, linalg.pinv2(np.dot(model.y_loadings.T, model.y_weights),**pinv2_args))
    else:
        model.y_rotations = np.ones(1)

    # Estimate regression coefficient
    # Regress Y on T
    # Y = TQ' + Err,
    # Then express in function of X
    # Y = X W(P'W)^-1Q' + Err = XB + Err
    # => B = W*Q' (p x q)
    model.coef = np.dot(model.x_rotations, model.y_loadings.T)
    model.coef = (1. / model.x_std.reshape((Np, 1)) * model.coef * model.y_std)


    # Calculate the SIMPLS weights (used for feature significance algorithms in post-training analysis)
    model = simpls_weights_from_nipals(model)

    return(model)


########################################################################################################################
# Trains a PLS predictive model using the NIPALS (Nonlinear Iterative Partial Least Squares) algorithm
#   This is the same thing as the more commonly used "power" method for computing eigenvectors
def pls_train_simpls(model, hp, Xin, Yin, pred_type = 'regression'):
    X = copy.deepcopy(Xin)
    Y = copy.deepcopy(Yin)
    ns = X.shape[0] # num training samples
    npred = X.shape[1] # num training predictors
    nr = Y.shape[1] # num responses
    n_comp = hp.n_comp

    # Scale (in place)
    if pred_type == 'regression' or 'classification':
        X, Y, model.x_mean, model.y_mean, model.x_std, model.y_std = standardize_xy(X, Y, hp.scale_data)
    else:
        model.x_mean = 0.0
        model.x_std = 1.0
        model.y_mean = 0.0
        model.y_std = 1.0

    # Residual (deflated) matrices
    # Xk = X
    # Yk = Y

    # Results matrices

    # SIMPLS algorithm
    S = np.dot(X.T, Y)
    P = np.zeros((npred, n_comp))
    T = np.zeros((ns, n_comp))
    R = np.zeros((npred, n_comp))
    for i in range(n_comp):

        if i == 0:
            U, Sigma, Vt = np.linalg.svd(S)
        else:
            Piprev = P[:,i-1].reshape((npred,1))
            pinvmat = np.linalg.pinv( np.dot(Piprev.T, Piprev))
            PiprevS = np.dot( Piprev.T, S)
            orthogonal_mat = np.dot( Piprev, np.dot( pinvmat, PiprevS))
            deflate_mat = S - orthogonal_mat
            U, Sigma, Vt = np.linalg.svd(deflate_mat)

        ri = U[:,0].reshape((npred,1)) # First left singular vector
        ti = np.dot(X, ri)
        tisq = np.dot(ti.T, ti)
        pi = np.dot(X.T, ti) / tisq

        P[:,i] = np.squeeze(pi)
        T[:,i] = np.squeeze(ti)
        R[:,i] = np.squeeze(ri)

    # Combine all of the above to get the PLS coefficients
    model.x_weights = copy.deepcopy(R)
    model.x_scores = copy.deepcopy(T)
    model.x_loadings = copy.deepcopy(P)

    pinv_TkTTk = np.linalg.pinv(np.dot( T.T,T))
    TkTY = np.dot(T.T, Y)
    model.coef = np.dot( R, np.dot(pinv_TkTTk, TkTY))

    return(model)



########################################################################################################################
# Converts weights from the NIPALS PLS algorithm to what they would be from the SIMPLS pls algorithm
def simpls_weights_from_nipals(model):

    loadings_wts = np.dot(model.x_loadings.T, model.x_weights)
    pinvmat = np.linalg.pinv(loadings_wts)
    model.x_weights_simpls = np.dot( model.x_weights, pinvmat)

    return(model)



########################################################################################################################
# Applies a previously trained NIPALS PLS model to new data
def apply_pls_nipals(model, X):
    # "Normalize" the predictors
    X -= model.x_mean
    X /= model.x_std

    # Apply the learned PLS coefficients to the normalized data
    Ypred = np.dot(X, model.coef)
    Ypred += model.y_mean

    return(Ypred)


########################################################################################################################
# Mean centers and rescales input, output data in preparation for PLS modeling
def standardize_xy(X, Y, scale=True):

    x_mean = X.mean(axis=0)
    X -= x_mean
    y_mean = Y.mean(axis=0)
    Y -= y_mean
    # scale
    if scale:
        x_std = X.std(axis=0, ddof=1)
        x_std[x_std == 0.0] = 1.0
        X /= x_std
        y_std = Y.std(axis=0, ddof=1)
        y_std[y_std == 0.0] = 1.0
        Y /= y_std
    else:
        x_std = np.ones(X.shape[1])
        y_std = np.ones(Y.shape[1])
    return X, Y, x_mean, y_mean, x_std, y_std



########################################################################################################################
# This is the NIPALS inner loop for computing singular vectors.  Basically the same thing as the power method for
#   computing eigenvectors, one at a time
def nipals_twoblocks_inner_loop(X, Y, mode="A", max_iter=500, tol=1e-06,
                                 norm_y_weights=False):
    """Inner loop of the iterative NIPALS algorithm.

    Provides an alternative to the svd(X'Y); returns the first left and right
    singular vectors of X'Y.  See PLS for the meaning of the parameters.  It is
    similar to the Power method for determining the eigenvectors and
    eigenvalues of a X'Y.
    """
    y_score = Y[:, [0]]
    x_weights_old = 0
    ite = 1
    X_pinv = Y_pinv = None
    eps = np.finfo(X.dtype).eps
    # Inner loop of the Wold algo.
    while True:
        # 1.1 Update u: the X weights
        if mode == "B":
            if X_pinv is None:
                # We use slower pinv2 (same as np.linalg.pinv) for stability
                # reasons
                X_pinv = linalg.pinv2(X, **pinv2_args)
            x_weights = np.dot(X_pinv, y_score)
        else:  # mode A
            # Mode A regress each X column on y_score
            x_weights = np.dot(X.T, y_score) / np.dot(y_score.T, y_score)

        # If y_score only has zeros x_weights will only have zeros. In
        # this case add an epsilon to converge to a more acceptable
        # solution
        if np.dot(x_weights.T, x_weights) < eps:
            x_weights += eps

        # 1.2 Normalize u
        x_weights /= np.sqrt(np.dot(x_weights.T, x_weights)) + eps

        # 1.3 Update x_score: the X latent scores
        x_score = np.dot(X, x_weights)

        # 2.1 Update y_weights
        if mode == "B":
            if Y_pinv is None:
                Y_pinv = linalg.pinv2(Y, **pinv2_args)  # compute once pinv(Y)
            y_weights = np.dot(Y_pinv, x_score)
        else:
            # Mode A regress each Y column on x_score
            y_weights = np.dot(Y.T, x_score) / np.dot(x_score.T, x_score)

        # 2.2 Normalize y_weights
        if norm_y_weights:
            y_weights /= np.sqrt(np.dot(y_weights.T, y_weights)) + eps

        # 2.3 Update y_score: the Y latent scores
        y_score = np.dot(Y, y_weights) / (np.dot(y_weights.T, y_weights) + eps)
        x_weights_diff = x_weights - x_weights_old
        if np.dot(x_weights_diff.T, x_weights_diff) < tol or Y.shape[1] == 1:
            break
        if ite == max_iter:
            warnings.warn('Maximum number of iterations reached')
            break
        x_weights_old = x_weights
        ite += 1
    return (x_weights, y_weights, ite)



########################################################################################################################
# This is a hack to ensure that the output of an SVD follows a consistent convention- so that the columns of u or v
#   that are largest in absolute value are always positive
#  Got this function from an external library- better to port here than to carry along an entirely new library
def svd_flip(u, v, u_based_decision=True):

    if u_based_decision:
        # columns of u, rows of v
        max_abs_cols = np.argmax(np.abs(u), axis=0)
        # signs = np.sign(u[max_abs_cols, xrange(u.shape[1])]) # Python 2.7
        signs = np.sign(u[max_abs_cols, range(u.shape[1])])  # Python 3.X
        u *= signs
        v *= signs[:, np.newaxis]
    else:
        # rows of v, columns of u
        max_abs_rows = np.argmax(np.abs(v), axis=1)
        # signs = np.sign(v[xrange(v.shape[0]), max_abs_rows]) # Python 2.7
        signs = np.sign(v[range(v.shape[0]), max_abs_rows])  # Python 3.X
        u *= signs
        v *= signs[:, np.newaxis]
    return (u, v)


########################################################################################################################
# Applies a previously trained NIPALS PLS model to new data to generate a PLS regression prediction
def apply_pls_regr(plsmodel, Xin):
    X = copy.deepcopy(Xin)

    # Normalize the predictors
    X -= plsmodel.x_mean
    X /= plsmodel.x_std
    Ypred = np.dot(X, plsmodel.coef)

    # Denormalize
    Ypred += plsmodel.y_mean

    # Convert predictions to multi-task list format to be compatible with other ML methods
    n_tasks = Ypred.shape[1]
    preds = []
    for i in range(n_tasks):
        preds.append(Ypred[:,i])

    return(preds)


########################################################################################################################
# Applies a previously trained NIPALS PLS model to new data to generate a PLS class prediction
def apply_pls_class(plsmodel, Xin, n_class, binary_thresh=None):
    n_s = np.size(Xin,0)
    n_feats = np.size(Xin,1)
    n_tasks = int(np.size(n_class))
    X = copy.deepcopy(Xin)

    # Normalize the predictors
    X -= plsmodel.x_mean
    X /= plsmodel.x_std
    Ypred = np.dot(X, plsmodel.coef)

    # Denormalize
    Ypred += plsmodel.y_mean

    # For each task, generate class predictions and probabilities of each class
    all_pred_probs = []
    all_class_pred = []
    for t in range(n_tasks):
        pred_probs = np.zeros((n_s, n_class[t]))
        pred_mat = np.zeros((n_s, n_class[t]))
        class_mat = np.zeros((n_s, n_class[t]))
        # Calculate the distance from the predicted value to each class
        for i in range(0, n_class[t]):
            pred_mat[:,i] = np.squeeze(Ypred[:,t])
            class_mat[:,i] = i

        delta_class = np.abs(pred_mat - class_mat)
        dist_class = np.sum(delta_class,1)
        dist_class[dist_class==0.0] = 0.000000001

        class_pred = np.zeros((n_s, 1))
        for i in range(0, n_s):
            pred_probs[i,:] = 1.0 / delta_class[i,:]
            pred_probs[i,:] /= np.sum(pred_probs[i,:])
            # If so desired, use a different class decision threshold for a binary classifier
            if n_class[t] == 2 and binary_thresh is not None:
                if pred_probs[i,1] >= binary_thresh[t]:
                    class_pred[i] = 1
                else:
                    class_pred[i] = 0
            else:
                # Take the highest probability prediction to be the class prediction
                max_prob = np.unique(np.max(pred_probs[i,:]))
                class_pred[i] = np.min(np.where(pred_probs[i,:] == max_prob))

        all_pred_probs.append(copy.deepcopy(pred_probs))
        all_class_pred.append(copy.deepcopy(class_pred))

    return(all_pred_probs, all_class_pred)


########################################################################################################################
# Base class for defining PLS specific hyperparameters
class PLS_HP():
    def __init__(self, pred_type = 'regression'):
        # self.pred_type = pred_type
        self.algo = 'nipals'
        self.max_iters = 1000
        self.norm_y_weights = False
        self.deflation_mode = 'regression' # can be regression or canonical
        self.n_comp = 3 # num components / latent vectors to use
        self.scale_data = False # whether or not to scale the input data
        self.tol = 1.0e-6 # convergence tolerance criteria for iterative PLS solution methods
        self.n_iters = copy.deepcopy(self.max_iters)


########################################################################################################################
class PLS_MODEL():
    def __init__(self):
        self.x_scores = None
        self.y_scores = None
        self.x_weights = None
        self.y_weights = None
        self.x_loadings = None
        self.y_loadings = None
        self.coef = None
        self.x_mean = None
        self.x_std = None
        self.y_mean = None
        self.y_std = None
        self.x_weights_simpls = None

