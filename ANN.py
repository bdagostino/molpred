# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

# Module for deep larning regression / classification training.  Uses Keras / Tensorflow

import numpy as np
import tensorflow.keras.optimizers
import tensorflow.keras.regularizers as regularizers
import MachineLearningUtils as ml
from tensorflow.keras.layers import Input, Dense, Masking, Dropout, BatchNormalization
import tensorflow.keras.models



########################################################################################################################
# Function to train an DNN classification model.  Uses the Keras library
def ann_train_class(hp, X_train, Y_train, weights = None, task_names = None):
    n_feats = int(np.size(X_train,1))
    n_tasks = int(np.size(Y_train,1))
    n_train = int(np.size(X_train,0))
    mask_value = -1

    imbalanced_weight_exp = 1.0

    uclasses, n_class = ml.unique_classes(Y_train, exclude_missing=True)
    print('Found %d classes in training data...' % (n_class))

    # Convert missing values to -1 to play well with downstream training algorithms
    Y_train[np.isnan(Y_train)] = mask_value

    if task_names is None:
        task_names = []
        for t in range(n_tasks):
            task_names.append('Task' + str(t))
    if len(task_names) != n_tasks:
        exit('Mismatch between # of tasks and # of task names!!!  ANN training cancelled')

    # Define the input layers (i.e. features)
    feats = Input(name='feats', shape=(n_feats,))
    dropout_feats = Dropout(0.2)(feats)
    print('n_feats = %d' % (n_feats) )

    # Set up the hidden layers
    init_dense = Dense(hp.n_neur, activation=hp.act_fn)(feats)
    init_dense = BatchNormalization()(init_dense)
    init_dense = Dropout(hp.dropout)(init_dense)
    for i in range(1, hp.n_hidden):
        print('defining hidden layer %d' % (i))
        # nneur_curlayer = int(hp.n_neur / (i + 1))
        nneur_curlayer = int(hp.n_hidden)  # trying this out
        init_dense = Dense(nneur_curlayer, activation=hp.act_fn, kernel_regularizer=regularizers.l2(0.001))(init_dense)
        init_dense = BatchNormalization()(init_dense)
        init_dense = Dropout(hp.dropout)(init_dense)

    # Build and compile model for classification
    # Multitask learning
    all_predictions = []
    all_train_labels = []
    all_sample_weights = []
    print('Defining per-task layers...')
    for t in range(n_tasks):
        cur_prediction = Dense(n_class[t], activation='softmax', name=task_names[t])(init_dense)

        # If desired, add in more layers on a per-task basis
        # task_dense = Dense(int(nneur / (1+n_hidden)), activation='relu', name=tasks[i]+'_dense')(init_dense)
        # task_dense = Dropout(0.5)(task_dense)
        # task_dense = BatchNormalization()(task_dense)
        # # task_dense = Dense(int(nneur / (2 + n_hidden)), activation='relu', name=tasks[i] + '_dense2')(task_dense)
        # # task_dense = Dropout(0.25)(task_dense)
        # # task_dense = BatchNormalization()(task_dense)
        # cur_prediction = Dense(nclass, activation='softmax', name=tasks[i])(task_dense)

        all_predictions.append(cur_prediction)

        if weights is None:
            curmaskidx = np.where(Y_train[:, t] == mask_value)[0]
            sample_weights = np.ones(n_train)
            sample_weights[curmaskidx] = 0
            ns_masked = np.size(curmaskidx)
            ns_unmasked = n_train - ns_masked
            ns_class = np.zeros(n_class[t])
            for c in range(0, n_class[t]):
                curidx = np.where(Y_train[:, t] == c)[0]
                ns_class[c] = np.size(curidx)
                inv_class_frac = float(ns_unmasked) / float(ns_class[c])
                sample_weights[curidx] = inv_class_frac

            sample_weights /= np.unique(np.max(sample_weights[sample_weights > 0]))
            sample_weights = sample_weights**imbalanced_weight_exp # nonlinear weights for smaller unbalanced classes
        else:
            # Sample weights defined externally to this function
            sample_weights = weights[:,t]

        print('Task: %s;  sample weights = %s' % (task_names[t], str(np.unique(sample_weights))))

        all_sample_weights.append(sample_weights)
        all_train_labels.append(ml.to_one_hot_encoding(Y_train[:, t], nclass=n_class[t]))

    model = tensorflow.keras.models.Model(inputs=[feats], outputs=all_predictions)


    # use_metrics = {'output_0': 'accuracy'}
    # use_metrics = {'output_0': 'balanced_accuracy'}
    use_metrics = {'output_0': 'accuracy', 'output_0': 'balanced_accuracy'}

    if hp.learning_rule.lower() == 'adam':
        use_opt = tensorflow.keras.optimizers.Adam(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'sgd':
        use_opt = tensorflow.keras.optimizers.SGD(lr=hp.learning_rate, momentum=hp.learning_momentum)
    elif hp.learning_rule.lower() == 'rmsprop':
        use_opt = tensorflow.keras.optimizers.RMSprop(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adagrad':
        use_opt = tensorflow.keras.optimizers.Adagrad(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adadelta':
        use_opt = tensorflow.keras.optimizers.Adadelta(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adamax':
        use_opt = tensorflow.keras.optimizers.Adamax(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'nadam':
        use_opt = tensorflow.keras.optimizers.Nadam(lr=hp.learning_rate)

    model.compile(loss='categorical_crossentropy', optimizer=use_opt, metrics=['accuracy'])

    print('model compiled.  about to hit model.fit()')
    model.fit(X_train, all_train_labels, epochs=hp.n_epochs, batch_size=hp.batch_size,
              validation_split=0.2, sample_weight=all_sample_weights)

    return(model)


########################################################################################################################
# Function to train an DNN regression model.  Uses the Keras library
def ann_train_regr(hp, X_train, Y_train, weights = None, task_names = None):
    n_feats = int(np.size(X_train,1))
    n_tasks = int(np.size(Y_train,1))
    n_train = int(np.size(X_train,0))
    mask_value = -1

    imbalanced_weight_exp = 1.0

    # Convert missing values to -1 to play well with downstream training algorithms
    Y_train[np.isnan(Y_train)] = mask_value

    if task_names is None:
        task_names = []
        for t in range(n_tasks):
            task_names.append('Task' + str(t))
    if len(task_names) != n_tasks:
        exit('Mismatch between # of tasks and # of task names!!!  DNN training cancelled')

    # Define the input layers (i.e. features)
    feats = Input(name='feats', shape=(n_feats,))
    # dropout_feats = Dropout(0.2)(feats)

    # Set up the hidden layers
    # init_dense = Dense(hp.n_neur, activation=hp.act_fn)(dropout_feats)
    init_dense = Dense(hp.n_neur, activation=hp.act_fn)(feats)
    init_dense = BatchNormalization()(init_dense)
    init_dense = Dropout(hp.dropout)(init_dense)
    for i in range(1, hp.n_hidden):
        nneur_curlayer = int(hp.n_neur / (i + 1))
        init_dense = Dense(nneur_curlayer, activation=hp.act_fn, kernel_regularizer=regularizers.l2(0.001))(init_dense)
        init_dense = BatchNormalization()(init_dense)
        init_dense = Dropout(hp.dropout)(init_dense)

    # Build and compile model for classification
    # Multitask learning
    all_predictions = []
    all_sample_weights = []
    all_train_values = []
    for t in range(n_tasks):
        cur_prediction = Dense(1, activation='linear', name=task_names[t], kernel_initializer='normal')(init_dense)

        # If desired, add in more layers on a per-task basis
        # task_dense = Dense(int(nneur / (1+n_hidden)), activation='relu', name=tasks[i]+'_dense')(init_dense)
        # task_dense = Dropout(0.5)(task_dense)
        # task_dense = BatchNormalization()(task_dense)
        # # task_dense = Dense(int(nneur / (2 + n_hidden)), activation='relu', name=tasks[i] + '_dense2')(task_dense)
        # # task_dense = Dropout(0.25)(task_dense)
        # # task_dense = BatchNormalization()(task_dense)
        # cur_prediction = Dense(nclass, activation='softmax', name=tasks[i])(task_dense)

        all_predictions.append(cur_prediction)

        if weights is None:
            # In regression models, give equal weight to all samples where we have activities
            curmaskidx = np.where(Y_train[:, t] == mask_value)[0]
            sample_weights = np.ones(n_train)
            sample_weights[curmaskidx] = 0
        else:
            # Sample weights defined externally to this function
            sample_weights = weights[:,t]

        print('Task: %s;  sample weights = %s' % (task_names[t], str(np.unique(sample_weights))))

        all_sample_weights.append(sample_weights)
        all_train_values.append(Y_train[:,t])

    model = tensorflow.keras.models.Model(inputs=[feats], outputs=all_predictions)

    use_metrics = {'output_0': 'mse'}

    if hp.learning_rule.lower() == 'adam':
        use_opt = tensorflow.keras.optimizers.Adam(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'sgd':
        use_opt = tensorflow.keras.optimizers.SGD(lr=hp.learning_rate, momentum=hp.learning_momentum)
    elif hp.learning_rule.lower() == 'rmsprop':
        use_opt = tensorflow.keras.optimizers.RMSprop(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adagrad':
        use_opt = tensorflow.keras.optimizers.Adagrad(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adadelta':
        use_opt = tensorflow.keras.optimizers.Adadelta(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'adamax':
        use_opt = tensorflow.keras.optimizers.Adamax(lr=hp.learning_rate)
    elif hp.learning_rule.lower() == 'nadam':
        use_opt = tensorflow.keras.optimizers.Nadam(lr=hp.learning_rate)


    model.compile(loss='mean_absolute_error', optimizer=use_opt, metrics=['mean_absolute_error'])
    model.summary()

    model.fit(X_train, all_train_values, epochs=hp.n_epochs, batch_size=hp.batch_size,
              validation_split=0.2, sample_weight=all_sample_weights)

    return(model)



########################################################################################################################
class ANN():
    def __init__(self):
        self.act_fn = None # what type of activation function to use in hidden layer?
        self.n_neur = None # number of neurons in "hidden" layer
        self.weights = None # weights in each layer
        self.biases = None # bias vectors in each layer


########################################################################################################################
#  Class for defining ANN specific hyperparameters
class ANN_HP():
    def __init__(self, pred_type='classification'):
        self.pred_type = pred_type # 'regression' or 'classification'
        self.n_neur = 1000
        self.act_fn = 'relu' # activation function (tanh, sigmoid, relu, etc.)
        self.learning_rate = 0.001 # ANN learning rate
        self.learning_rule = 'adam' # sgd, nesterov, momentum, adadelta, adagrad, rmsprop
        self.learning_momentum = 0.9 # momentum parameter, if using a momentum based learning rule
        self.n_iters = 100 # max num iters to perform
        self.n_hidden = 1 # num hidden layers in the network
        self.dropout = 0.5 # fraction of neurons to drop out during training
        self.batch_size = 20 # batch size to use for training
        self.n_epochs = 10 # number of epochs for each training cycle

