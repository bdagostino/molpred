# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'

# This module implements functions for training / applying KNN based machine learning algorithms

import MachineLearningUtils as ml
import numpy as np
import copy

########################################################################################################################
# Trains a kNN model
def knn_train( model, hp, X_train, Y_train, pred_type='classification' ):
    n_tasks = int(np.size(Y_train,1))
    hp.ns = np.size(X_train,0)
    hp.npred = np.size(X_train,1)
    if pred_type == 'classification':
        model.n_class = np.zeros(n_tasks, dtype=np.int32)
        for t in range(n_tasks):
            ulabels = np.unique(Y_train[:,t]) # unique class labels
            model.n_class[t] = np.size(ulabels)
    model.X_train = copy.deepcopy(X_train)
    model.Y_train = copy.deepcopy(Y_train)

    model.hp = copy.deepcopy(hp)
    return(model)


########################################################################################################################
# Applies a trained KNN regression model to a number of samples in the training set
def apply_knn_regr(model, X_test):

    ns_test = np.size(X_test,0)
    npred_test = np.size(X_test,1) # number of predictors in the test data
    ns_model = np.size(model.X_train,0)
    npred_model = np.size(model.X_train,1)
    Y_pred = np.zeros((ns_test,1))

    if npred_test != npred_model:
        exit('Unequal number of predictors in the test and KNN training data!!!')

    for i in range(0, ns_test):
        alldist = np.zeros(ns_model)
        for j in range(0, ns_model):
            alldist[j] = ml.vector_distance(X_test[i,:], model.X_train[j,:], model.hp.distance_type)

        # Identify the k closest neighbors
        sortidx = np.argsort(alldist)
        closestidx = sortidx[0:model.hp.k]
        closest_preds = model.Y_train[closestidx,:]
        closest_dist = alldist[closestidx]
        closest_weights = assign_weights(closest_dist, model.hp.weight_type)
        Y_pred[i,0] = np.vdot( closest_weights, closest_preds)

    return(Y_pred)


########################################################################################################################
# Applies a trained KNN predictive model to generate classification predictions
def apply_knn_class(model, X_test, binary_thresh=None):
    ns_test = np.size(X_test,0)
    npred_test = np.size(X_test,1) # number of predictors in the test data
    ns_model = np.size(model.X_train,0)
    npred_model = np.size(model.X_train,1)
    n_tasks = int(np.size(model.Y_train,1))
    class_pred = []
    prob_preds = []
    for t in range(n_tasks):
        class_pred.append(np.zeros((ns_test,1)))
        prob_preds.append(np.zeros((ns_test,model.n_class[t])))

    if npred_test != npred_model:
        exit('Unequal number of predictors in the test and KNN training data!!!')

    for i in range(0, ns_test):
        alldist = np.zeros(ns_model)
        for j in range(0, ns_model):
            alldist[j] = ml.vector_distance(X_test[i,:], model.X_train[j,:], model.hp.distance_type)

        # Identify the k closest neighbors
        sortidx = np.argsort(alldist)
        closestidx = sortidx[0:model.hp.k]

        for t in range(n_tasks):
            closest_preds = model.Y_train[closestidx,t]
            closest_dist = alldist[closestidx]
            closest_weights = assign_weights(closest_dist, model.hp.weight_type)

            # Count the number of votes from each of the k nearest neighbors
            score_classes = np.zeros(model.n_class[t])
            closest_preds = np.int32(closest_preds)
            for k in range(0, model.n_class[t]):
                curcloseidx = np.where( closest_preds == k)[0]
                predincurclass = np.zeros( model.hp.k)
                predincurclass[curcloseidx] = 1.0
                score_classes[k] = np.vdot( closest_weights, predincurclass )
            max_scores = np.max(score_classes)
            try:
                maxidx = np.min(np.where(score_classes == max_scores)[0])
            except:
                print('debug point here...')
            class_pred[t][i,0] = maxidx

            #Convert the class predictions to probabilities
            prob_preds[t][i,:] = knn_class_probs(closest_preds, closest_dist, model.hp.weight_type, model.n_class[t])

            # If so desired, use a different class decision threshold for a binary classifier
            if model.n_class[t] == 2 and binary_thresh is not None:
                if prob_preds[t][i,1] >= binary_thresh[t]:
                    class_pred[t][i,0] = 1
                else:
                    class_pred[t][i,0] = 0

    #TODO: add in a class balancing term that gives more weight to predictions from smaller classes in the training set

    return(prob_preds, class_pred)


########################################################################################################################
# Assign probabilities to KNN class predictions for a single sample
#   Method of assigning probability depends on weighting type
def knn_class_probs(class_preds, dist_samples, weight_type, nclass):

    ninclass = np.zeros(nclass)
    probs = np.zeros(nclass)
    ns = np.size(class_preds)
    for i in range(0, nclass):
        ninclass[i] = np.size(np.where(class_preds == i)[0])

    if weight_type == 'uniform':
        probs = ninclass / float(ns)

    elif weight_type == 'distance':
        # Closer samples receive proportionally more weight
        distvals = np.abs(dist_samples)
        sumdistvals = np.sum(distvals)
        weights = 1.0 - (distvals / sumdistvals)
        weights /= np.sum(weights)
        for i in range(0, nclass):
            curclassidx = np.where( class_preds == i)[0]
            probs[i] = np.sum(weights[curclassidx])

    return( probs )


########################################################################################################################
# Assigns weights to distances as per predefined weighting types.  It is understood that lower distances receive higher
#   weights
def assign_weights( distances, weight_type, normalize = True):
    ns = np.size(distances)

    if ns == 1:
        weights = np.array([1.0])
        return(weights)

    if weight_type == 'uniform':
        # All samples receive equal weight
        weights = np.ones(ns)
        if normalize == True:
            weights /= float(ns)
    elif weight_type == 'distance':
        # Closer samples receive proportionally more weight (proportional to 1/distance)
        distvals = np.abs(distances)
        sumdistvals = np.sum(distvals)
        if sumdistvals <= 0.0:
            weights = np.ones(ns)
        else:
            weights = 1.0 - (distvals / sumdistvals)
        if normalize == True:
            weights /= np.sum(weights)

    return( weights )


########################################################################################################################
########################################################################################################################
########################################################################################################################
# Base class for defining KNN specific hyperparameters
class KNN_HP():
    def __init__(self):
        self.k = 3
        self.n_s = 0 # number of samples in the training set.  Gets overwritten based on what's in the training set
        self.n_feats = 0 # number of predictors in the training set.  Gets overwritten based on what's in the training set
        self.distance_type = 'euclidean' # what type of distance metric for determining 'closeness'?  euclidean, hamming, mahalanobis
        self.weight_type = 'uniform' # 'uniform' weights to all samples or 'distance' based


########################################################################################################################
########################################################################################################################
########################################################################################################################
class KNN_MODEL():
    def __init__(self):
        self.X_train = None
        self.Y_train = None
        self.x_mean = None
        self.x_stddev = None
        self.x_cov = None

