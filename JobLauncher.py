# Copyright (c) 2020, Robert D'Agostino
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Robert D'Agostino nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Robert D'Agostino BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

__author__ = 'bob dagostino'


########################################################################################################################
# This module provides functions for packaging up and launching jobs.  Note that it does not depend on any other MolPred
#   modules.  This is done to ensure that jobs on Docker or AWS can be run without installing heavy dependencies

import os, glob, copy, time
import shutil
import xml.etree.ElementTree as et
import boto3


########################################################################################################################
# Run the job on AWS
def run_job_aws(config_file, job_date=None, job_time=None):

    if job_date is None or job_time is None:
        job_time = time.strftime('%H-%M-%S')
        job_date = time.strftime('%m%d%Y')

    # Build the payload to send to docker via command line
    full_payload_file, save_root = build_docker_payload(config_file, job_date, job_time)
    payload_root, base_payload = os.path.split(full_payload_file)

    # Get the upload and AWS Batch information from the xml file
    tree = et.parse(config_file)
    root = tree.getroot()
    tokens = base_payload.split('.zip')
    job_name = tokens[0]
    bucket_name = root.find('AWS_SETTINGS').find('s3_bucket_name').text
    incoming_jobs_folder = root.find('AWS_SETTINGS').find('incoming_jobs_folder').text
    completed_jobs_folder = root.find('AWS_SETTINGS').find('completed_jobs_folder').text
    aws_job_queue = root.find('AWS_SETTINGS').find('aws_job_queue').text
    aws_job_definition = root.find('AWS_SETTINGS').find('aws_job_definition').text
    recipient_email = root.find('AWS_SETTINGS').find('recipient_email').text

    # Upload the job payload to an S3 bucket
    # NOTE: DON'T use path.join() for this- Windows will modify the path separators to be different from what AWS needs!
    cloud_src_root = 's3://' + bucket_name + '/' + incoming_jobs_folder
    cloud_dst_dir = 's3://' + bucket_name + '/' + completed_jobs_folder
    cloud_src_dir = cloud_src_root + '/' + base_payload

    print('\033[1;45mUploading job payload %s to S3:  %s \033[1;m' % (full_payload_file, cloud_src_root))
    s3 = boto3.resource('s3')
    s3.meta.client.upload_file(full_payload_file, bucket_name, incoming_jobs_folder + '/' + base_payload)
    print('\033[1;45m... S3 upload complete\033[1;m')

    # Build the containerOverrides bash command to submit to container at run time
    run_cmd = ['python', 'RunMolPredDockerJob.py', base_payload, '--cloud_src_dir', cloud_src_dir, '--cloud_dst_dir',
               cloud_dst_dir]
    if recipient_email is not None:
        run_cmd.append('--recipient')
        run_cmd.append(recipient_email)

    # Submit a job to AWS Batch
    #TODO: logic for which type of job / compute environment to use (CPU vs. GPU)
    # This is to run a CPU job on AWS
    print('\033[1;46\n\nSubmitting job to AWS Batch...\033[1;m')
    client = boto3.client('batch')
    client.submit_job(jobName=job_name,
                      jobQueue=aws_job_queue,
                      jobDefinition=aws_job_definition,
                      containerOverrides={'command': run_cmd}
                      )
    print('\033[1;46... AWS Batch job submitted\033[1;m')
    if recipient_email is not None:
        print('When the AWS job completes, an email will be sent to %s with a download link to the results folder' % (recipient_email))

    return(True)



########################################################################################################################
# Run the job in a Docker container that contains MolPred
def run_job_docker(config_file, docker_image_name, job_date = None, job_time = None):

    if job_date is None or job_time is None:
        job_time = time.strftime('%H-%M-%S')
        job_date = time.strftime('%m%d%Y')

    # Build the payload to send to docker via command line
    payload_name, save_root = build_docker_payload(config_file, job_date, job_time)
    payload_root, base_payload = os.path.split(payload_name)

    # Create a writeable "volume" on the MolPred Docker container
    volume_name = payload_name + ':/~/MolPred/' + base_payload
    curtime = time.strftime('%H-%M-%S')
    assigned_container_name = 'molpred_' + curtime
    copied_job_name = 'DockerJob_' + curtime + '.zip'
    python_args = '/~/MolPred/' + base_payload + ' --local_dst_file ' + copied_job_name

    runvol_cmd = 'docker run -v ' + volume_name + ' -t --name ' + assigned_container_name + ' ' + docker_image_name + ' python RunMolPredDockerJob.py ' + python_args

    os.system(runvol_cmd)

    # Move the results folder from the Docker container to the local destination directory
    copy_local_cmd = 'docker cp ' + assigned_container_name + ':/~/MolPred/'+copied_job_name + ' ' + save_root

    os.system(copy_local_cmd)

    # Delete the no longer needed Docker payload .zip file from local computer.  All data is already contained in the
    #   Docker results folder.  Also remove unzipped payload folder
    os.remove(payload_name)
    base_payload_unzipped, _ = os.path.splitext(base_payload)
    unzipped_payload_name = os.path.join(payload_root, base_payload_unzipped)
    shutil.rmtree(unzipped_payload_name)

    # Remove the original config file as it is also contained in the Docker results folder
    # os.remove(config_file)

    # Prune exited containers
    os.system('docker container prune --force')

    # Get rid of the container volume as it doesn't need to persist
    os.system('docker volume prune --force')

    # Remove "dangling" images from crashed or stopped containers that are no longer in use.  Dangling images consume
    #   roughly 8 GB of memory each!  End users shouldn't have this problem though...
    os.system('docker rmi $(docker images --quiet --filter "dangling=true")')

    return(True)



# ########################################################################################################################
# Packages data for running a job in a Docker container.  Specifics depend on the type of job to run
def build_docker_payload(config_file, job_date_str, job_time_str):

    tree = et.parse(config_file)
    root = tree.getroot()
    job_type = root.attrib['job_type']

    # local_paths = LOCAL_PATHS()
    if job_type == 'training' or job_type == 'model_stress_test':
        save_root = root.find('IO_PATHS').find('save_root').text
    elif job_type == 'inference':
        save_root = root.find('save_root').text

    # Make the payload folder
    payload_folder = os.path.join(save_root, 'docker_payload_' + job_date_str + '_' + job_time_str)
    if os.path.exists(payload_folder):
        shutil.rmtree(payload_folder, ignore_errors=True)
    print('Creating job payload folder: %s' % (payload_folder))
    try:
        os.mkdir(payload_folder)
    except:
        exit('Failed to create payload folder:  %s' % (payload_folder))

    if job_type == 'training' or job_type == 'model_stress_test':
        success = build_payload_training_mst(payload_folder, config_file, job_type)
    elif job_type == 'inference':
        success = build_payload_inference(payload_folder, config_file)
    else:
        exit('\n!!! Unknown job type:  %s !!!' % (job_type))

    # Zip the folder
    payload_name = payload_folder + '.zip'
    try:
        shutil.make_archive(payload_folder, 'zip', payload_folder)
    except:
        exit('Failed compressing Docker payload folder:  %s' % (payload_name))

    return(payload_name, save_root)



# ########################################################################################################################
# Builds a Docker payload for an inference job
def build_payload_inference(payload_folder, config_file):

    tree = et.parse(config_file)
    root = tree.getroot()

    # IO Paths on local machine
    molecules_file = root.find('molecules_file').text
    mol_root = root.find('mol_root').text
    excludes_file = root.find('excludes_file').text

    # Get the molecule names, including for the external test set (if used)
    try:
        f = open(molecules_file,'r')
        lines = f.readlines()
        f.close()
    except:
        exit('Could not read molecule names from molecules file: %s' % (molecules_file))

    mol_names = []
    for curline in lines[1:]:
        tokens = curline.rstrip('\r\n').split(',')
        if len(tokens[0]) > 0:
            mol_names.append(tokens[0])

    # Given the needed molecule names, move them to the folder within the payload root
    n_mols = len(mol_names)

    mol_folder = os.path.join(payload_folder,'mol')
    try:
        os.mkdir(mol_folder)
    except:
        exit('Could not create folder:  %s' % (mol_folder))

    missing_mol_files = []

    for i in range(n_mols):
        base_mol = mol_names[i].upper() + '.mol'
        orig_mol_file = os.path.join(mol_root, base_mol)
        moved_mol_file = os.path.join(mol_folder,base_mol)
        if os.path.exists(orig_mol_file):
            shutil.copy(orig_mol_file, moved_mol_file)
        else:
            missing_mol_files.append(base_mol)

    # Copy molecules file (optionally, the excludes file) to payload folder
    activity_folder = os.path.join(payload_folder,'activity')
    activity_root, base_molecules_file = os.path.split(molecules_file)
    try:
        os.mkdir(activity_folder)
    except:
        exit('Could not create folder:  %s' % (activity_folder))

    try:
        shutil.copy(molecules_file, os.path.join(activity_folder, base_molecules_file))
    except:
        exit('Could not move molecules file:  %s' % (molecules_file))

    if excludes_file is not None:
        try:
            excludes_root, base_excludes_file = os.path.split(excludes_file)
            shutil.copy(excludes_file, os.path.join(activity_folder, base_excludes_file))
        except:
            exit('Could not move excludes file:  %s' % (excludes_file))


    # Copy the models to the models subfolder within the payload folder
    models_folder = os.path.join(payload_folder, 'models')
    try:
        os.mkdir(models_folder)
    except:
        exit('Could not create folder:  %s' % (models_folder))
    model_files_root = root.find('MODEL_FILES')
    base_model_files = []
    for curmodel_file in model_files_root:
        curmodel_root, base_model_file = os.path.split(curmodel_file.text)
        shutil.copy(curmodel_file.text, os.path.join(models_folder,base_model_file))
        base_model_files.append(base_model_file)

        # Copy over any .h5 files needed for ANNs
        curh5_files = glob.glob(curmodel_root + '/*.h5')
        for h5file in curh5_files:
            _, base_h5_model_file = os.path.split(h5file)
            shutil.copy(h5file, os.path.join(models_folder, base_h5_model_file))


    # Write missing mol files to separate files
    missing_mols_file = os.path.join(payload_folder, 'missing_mol_files.csv')
    try:
        f = open(missing_mols_file,'w')
        for i in range(len(missing_mol_files)):
            f.write('%s\r\n' % (missing_mol_files[i]))
        f.close()
    except:
        exit('Failed writing file:  %s' % (missing_mols_file))

    # Copy the job .xml file to the payload folder, and then edit the file to run locally (while in the Docker container)
    config_root, base_config_file = os.path.split(config_file)
    payload_config_file = os.path.join(payload_folder, base_config_file)
    try:
        shutil.copy(config_file, payload_config_file)
    except:
        exit('Could not move config file %s from %s  to  %s' % (base_config_file, config_root, payload_folder))

    #  Also edit it to run with the paths defined above for results
    element_tree = et.parse(payload_config_file)
    root = element_tree.getroot()
    docker_mol_root = '/~/MolPred/mol'
    docker_save_root = '/~/MolPred/'
    docker_models_root = '/~/MolPred/models'
    docker_molecules_file = '/~/MolPred/activity/' + base_molecules_file

    if excludes_file is not None:
        docker_excludes_file = '/~/MolPred/activity/' + base_excludes_file
        root.find('IO_PATHS').find('excludes_file').text = docker_excludes_file
    else:
        docker_excludes_file = ''

    root.find('mol_root').text = docker_mol_root
    root.find('save_root').text = docker_save_root
    root.find('molecules_file').text = docker_molecules_file
    root.find('excludes_file').text = docker_excludes_file

    model_files_root = root.find('MODEL_FILES')
    i = 0
    for model_file in model_files_root:
        model_file.text = docker_models_root + '/' + base_model_files[i]
        i += 1

    # In the Docker config file, eliminate the Docker image to avoid infinite container recursion!
    root.find('docker_image_name').text = ''
    root.find('run_aws_job').text = ''

    # Write the xml changes to file
    element_tree.write(payload_config_file)

    return(True)


# ########################################################################################################################
# Builds a Docker payload for a training or a model stress test job
def build_payload_training_mst(payload_folder, config_file, job_type):

    tree = et.parse(config_file)
    root = tree.getroot()

    # IO Paths on local machine
    activity_file = root.find('IO_PATHS').find('activity_file').text
    mol_root = root.find('IO_PATHS').find('mol_root').text
    excludes_file = root.find('IO_PATHS').find('excludes_file').text

    if job_type == 'training':
        test_set_file = root.find('IO_PATHS').find('test_set_file').text
    else:
        test_set_file = None

    # Get the molecule names, including for the external test set (if used)
    try:
        f = open(activity_file,'r')
        lines = f.readlines()
        f.close()
    except:
        exit('Could not read sample names from activity file: %s' % (activity_file))

    mol_names = []
    for curline in lines[1:]:
        tokens = curline.rstrip('\r\n').split(',')
        if len(tokens[0]) > 0:
            mol_names.append(tokens[0])

    if test_set_file is not None:
        try:
            f_test = open(test_set_file,'r')
            lines = f_test.readlines()
            f_test.close()
            for curline in lines[1:]:
                tokens = curline.rstrip('\r\n').split(',')
                if len(tokens[0]) > 0:
                    mol_names.append(tokens[0])
        except:
            exit('Could not read sample names from test set file: %s' % (test_set_file))


    # Given the needed molecule names, move them to the folder within the payload root
    n_mols = len(mol_names)

    mol_folder = os.path.join(payload_folder,'mol')
    try:
        os.mkdir(mol_folder)
    except:
        exit('Could not create folder:  %s' % (mol_folder))

    missing_mol_files = []

    for i in range(n_mols):
        base_mol = mol_names[i].upper() + '.mol'
        orig_mol_file = os.path.join(mol_root, base_mol)
        moved_mol_file = os.path.join(mol_folder,base_mol)
        if os.path.exists(orig_mol_file):
            shutil.copy(orig_mol_file, moved_mol_file)
        else:
            missing_mol_files.append(base_mol)

    # Move activity (optionally, test_set_file and excludes file) to payload folder
    activity_folder = os.path.join(payload_folder,'activity')
    activity_root, base_activity_file = os.path.split(activity_file)
    try:
        os.mkdir(activity_folder)
    except:
        exit('Could not create folder:  %s' % (activity_folder))

    try:
        shutil.copy(activity_file, os.path.join(activity_folder, base_activity_file))
    except:
        exit('Could not move activity file:  %s' % (activity_file))

    if test_set_file is not None:
        try:
            test_set_root, base_test_set_file = os.path.split(test_set_file)
            shutil.copy(test_set_file, os.path.join(activity_folder, base_test_set_file))
        except:
            exit('Could not move test set file:  %s' % (test_set_file))

    if excludes_file is not None:
        try:
            excludes_root, base_excludes_file = os.path.split(excludes_file)
            shutil.copy(excludes_file, os.path.join(activity_folder, base_excludes_file))
        except:
            exit('Could not move excludes file:  %s' % (excludes_file))


    # Write missing mol files to separate files
    missing_mols_file = os.path.join(payload_folder, 'missing_mol_files.csv')
    try:
        f = open(missing_mols_file,'w')
        for i in range(len(missing_mol_files)):
            f.write('%s\r\n' % (missing_mol_files[i]))
        f.close()
    except:
        exit('Failed writing file:  %s' % (missing_mols_file))

    # Copy the job .xml file to the payload folder, and then edit the file to run locally (while in the Docker container)
    config_root, base_config_file = os.path.split(config_file)
    payload_config_file = os.path.join(payload_folder, base_config_file)
    try:
        shutil.copy(config_file, payload_config_file)
    except:
        exit('Could not move config file %s from %s  to  %s' % (base_config_file, config_root, payload_folder))

    #  Also edit it to run with the paths defined above for results
    element_tree = et.parse(payload_config_file)
    root = element_tree.getroot()
    docker_mol_root = '/~/MolPred/mol'
    docker_save_root = '/~/MolPred/'
    docker_activity_file = '/~/MolPred/activity/'+ base_activity_file
    if test_set_file is not None:
        docker_test_set_file = '/~/MolPred/activity/' + base_test_set_file
        root.find('IO_PATHS').find('test_set_file').text = docker_test_set_file
    else:
        docker_test_set_file = ''

    if excludes_file is not None:
        docker_excludes_file = '/~/MolPred/activity/' + base_excludes_file
        root.find('IO_PATHS').find('excludes_file').text = docker_excludes_file
    else:
        docker_excludes_file = ''

    root.find('IO_PATHS').find('mol_root').text = docker_mol_root
    root.find('IO_PATHS').find('save_root').text = docker_save_root
    root.find('IO_PATHS').find('activity_file').text = docker_activity_file
    root.find('IO_PATHS').find('excludes_file').text = docker_excludes_file
    if job_type == 'training':
        root.find('IO_PATHS').find('test_set_file').text = docker_test_set_file

    # In the Docker config file, eliminate the Docker image to avoid infinite container recursion!
    root.find('docker_image_name').text = ''
    root.find('run_aws_job').text = ''

    # Write the xml changes to file
    element_tree.write(payload_config_file)

    return(True)

